package purnama.yahya.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "appx09modif"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tVideo= "create table video(id_video text primary key, id_cover text not null, video_title text not null)"
        val insVideo= "insert into video(id_video,id_cover,video_title) values " +
                "('0x7f0c0000','0x7f06005f','Alan Walker - On My Way')," +
                "('0x7f0c0001','0x7f060069','Magic - Rude'),"
        db?.execSQL(tVideo)
        db?.execSQL(insVideo)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}