package purnama.yahya.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener{

    override fun onClick(v: View?) {

    }

    var posVidSkrg = 0
    lateinit var mediaController: MediaController
    lateinit var db : SQLiteDatabase
    lateinit var lsAdapter : ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = DBOpenHelper(this).writableDatabase
        mediaController = MediaController(this)
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        lvVideo.setOnItemClickListener(itemClick)
    }

    fun showDataVid(){
        val cursor : Cursor = db.query("video", arrayOf("id_video as _id","id_cover","video_title"),
            null,null,null,null,"video_title asc")
        lsAdapter = SimpleCursorAdapter(this,R.layout.item_data_video,cursor,
            arrayOf("_id","id_cover","video_title"), intArrayOf(R.id.txIdVideo, R.id.txIdCover, R.id.txVideoTitle),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lvVideo.adapter = lsAdapter
    }

    override fun onStart() {
        super.onStart()
        showDataVid()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        txJudulVideo.setText(c.getString(c.getColumnIndex("video_title")))
        imV.setImageResource(c.getInt(c.getColumnIndex("id_cover")))
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
    }

    var nextVid = View.OnClickListener { v:View ->
        if(posVidSkrg<(lvVideo.adapter.count-1)) posVidSkrg++
        else posVidSkrg = 0
        videoSet(posVidSkrg)
    }

    var prevVid = View.OnClickListener { v:View ->
        if(posVidSkrg>0) posVidSkrg--
        else posVidSkrg = lvVideo.adapter.count-1
        videoSet(posVidSkrg)
    }

    fun videoSet(pos: Int){
        val c: Cursor = lvVideo.adapter.getItem(pos) as Cursor
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var video_title = c.getString(c.getColumnIndex("video_title"))
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
        imV.setImageResource(id_cover)
        txJudulVideo.setText(video_title)
    }
}